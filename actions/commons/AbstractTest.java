package commons;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.Reporter;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.InternetExplorerDriverManager;

public class AbstractTest {
	WebDriver driver;
	protected final Log log;

	protected AbstractTest() {
		log = LogFactory.getLog(getClass());
	}

	private boolean checkPassed(boolean condition) {
		boolean pass = true;
		try {
			log.info("---------- PASSED ----------");
			Assert.assertTrue(condition);
		} catch (Throwable e) {
			pass = false;
			log.info("---------- FAILED ----------");
			// Attach exception to ReportNG
			Reporter.getCurrentTestResult().setThrowable(e);
		}
		return pass;
	}

	protected boolean verifyTrue(boolean condition) {
		log.info("---------- Verify True ----------");
		return checkPassed(condition);

	}

	private boolean checkFailed(boolean condition) {
		boolean pass = true;
		try {
			Assert.assertFalse(condition);
		} catch (Throwable e) {
			pass = false;
			Reporter.getCurrentTestResult().setThrowable(e);
		}
		return pass;
	}

	protected boolean verifyFalse(boolean condition) {
		return checkFailed(condition);
	}

	private boolean checkEquals(Object actual, Object expected) {
		boolean pass = true;
		try {
			Assert.assertEquals(actual, expected);
		} catch (Throwable e) {
			pass = false;
			Reporter.getCurrentTestResult().setThrowable(e);
		}
		return pass;
	}

	protected boolean verifyEquals(Object actual, Object expected) {
		return checkEquals(actual, expected);
	}

	protected void closeBrowser(WebDriver driver) {
		try {
			String osName = System.getProperty("os.name").toLowerCase();
			String cmd = "";
			driver.quit();
			if (driver.toString().toLowerCase().contains("chrome")) {
				if (osName.toLowerCase().contains("mac")) {
					cmd = "pkill chromedriver";
				} else {
					cmd = "taskkill /IM chromedriver.exe /F";
					// M�?i verison dùng
					// cmd = "taskkill /F /FI \"IMAGENAME eq chromedriver*\"";
				}
				Process process = Runtime.getRuntime().exec(cmd);
				process.waitFor();
			}
			if (driver.toString().toLowerCase().contains("internetexplorer")) {
				cmd = "taskkill /IM IEDriverServer.exe /F";
				// m�?i version dùng
				// cmd = "taskkill /F /FI \"IMAGENAME eq IEDriverServer*\"";
				Process process = Runtime.getRuntime().exec(cmd);
				process.waitFor();
			}
			log.info("---------- QUIT BROWSER SUCCESS ----------");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public WebDriver openMultiBrowser(String browserName) {

		if (browserName.equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browserName.equals("firefox")) {
			driver = new FirefoxDriver();
		} else if (browserName.equals("chrome_headless")) {
			System.setProperty("webdriver.chrome.driver", ".\\resources\\chromedriver.exe");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("headless");
			options.addArguments("window-size=1920x1080");
			driver = new ChromeDriver(options);
		} else if (browserName.equals("ie11")) {
			System.setProperty("webdriver.ie.driver", ".\\resources\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();

		} else {
			System.out.println("Can't init browser!");
		}
		driver.get("http://demo.guru99.com/v4");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		return driver;
	}

	public WebDriver openMultiBrowsersWithVersion(String browserName, String browserVersion) {
		if (browserName.equals("chrome")) {
			// System.setProperty("webdriver.chrome.driver",
			// ".\\resources\\chromedriver.exe");
			ChromeDriverManager.getInstance().version(browserVersion).setup();
			driver = new ChromeDriver();
		} else if (browserName.equals("firefox")) {
			// Firefox version 47.0.2 + Selenium 2.53.1 -> Dont need gecko driver
			FirefoxDriverManager.getInstance().version(browserVersion).setup();
			driver = new FirefoxDriver();
		} else if (browserName.equals("chrome_headless")) {
			// System.setProperty("webdriver.chrome.driver",
			// ".\\resources\\chromedriver.exe");
			ChromeDriverManager.getInstance().version(browserVersion).setup();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("headless");
			options.addArguments("window-size=1920x1080");
			driver = new ChromeDriver(options);
		} else if (browserName.equals("ie11")) {
			// System.setProperty("webdriver.ie.driver",
			// ".\\resources\\IEDriverServer.exe");
			InternetExplorerDriverManager.getInstance().version(browserVersion).setup();
			driver = new InternetExplorerDriver();
		} else {
			System.out.println("Can't init browser!");
		}

		driver.get("http://demo.guru99.com/v4");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		return driver;

	}

	public static int emailRandom() {
		Random random = new Random();
		int number = random.nextInt(999999);
		return number;

	}
}
