package commons;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import bankguru.AbstractPageUI;
import pageObjects.BalancePageObject;
import pageObjects.DeleteAccountPageObject;
import pageObjects.DeleteCustomerPageObject;
import pageObjects.DepositPageObject;
import pageObjects.EditCustomerPageObject;
import pageObjects.FunTransferPageObject;
import pageObjects.HomePageObject;
import pageObjects.NewAccountPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.PageManageDriver;
import pageObjects.WithdrawalPageObject;

public class AbstractPage {

	protected final Log log;

	protected AbstractPage() {
		log = LogFactory.getLog(getClass());
	}

	public void openAnyUrl(WebDriver driver, String url) {
		driver.get(url);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	public String getTitle(WebDriver driver) {
		return driver.getTitle();
	}

	public String getCurrentUrl(WebDriver driver) {
		return driver.getCurrentUrl();
	}

	public String getPageSource(WebDriver driver) {
		return driver.getPageSource();
	}

	public void back(WebDriver driver) {
		driver.navigate().back();
	}

	public void foward(WebDriver driver) {
		driver.navigate().forward();
	}

	public void refresh(WebDriver driver) {
		driver.navigate().refresh();
	}

	public void clickToElement(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		element.click();
	}

	public void clickToElement(WebDriver driver, String locator, String... value) {
		locator = String.format(locator, (Object[]) value);
		WebElement element = driver.findElement(By.xpath(locator));
		element.click();
		log.info("---------- Clicked to element = " + locator);
	}

	public void sendKeyToElement(WebDriver driver, String locator, String value) {
		WebElement element = driver.findElement(By.xpath(locator));
		element.sendKeys(value);

	}

	public void notClearAndSendKeyToElement(WebDriver driver, String locator, String inputValue, String... value) {
		locator = String.format(locator, (Object[]) value);
		WebElement element = driver.findElement(By.xpath(locator));
		element.sendKeys(inputValue);
		log.info("---------- Sendkey to element = " + locator);

	}

	public void ClearAndSendKeyToElement(WebDriver driver, String locator, String inputValue, String... value) {
		locator = String.format(locator, (Object[]) value);
		WebElement element = driver.findElement(By.xpath(locator));
		element.clear();
		element.sendKeys(inputValue);
		log.info("---------- Clear and Sendkey to element = " + locator);

	}

	public void selectItemInDropdown(WebDriver driver, String locator, String textItem) {
		WebElement element = driver.findElement(By.xpath(locator));
		Select select = new Select(element);
		select.selectByVisibleText(textItem);

	}

	public String getFirstSelectItem(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		Select select = new Select(element);
		return select.getFirstSelectedOption().getText();
	}

	public String getAttribute(WebDriver driver, String locator, String attribute) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.getAttribute(attribute);
	}

	public String getTextElement(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		log.info("---------- Get text for = " + locator);
		return element.getText();
		
	}

	public String getTextElement(WebDriver driver, String locator, String... value) {
		locator = String.format(locator, (Object[]) value);
		WebElement element = driver.findElement(By.xpath(locator));
		log.info("---------- Get text for = " + locator);
		return element.getText();
	}

	public int getSizeElement(WebDriver driver, String locator) {
		List<WebElement> elements = driver.findElements(By.xpath(locator));
		return elements.size();

	}

	public void checkToCheckbox(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		if (!element.isSelected()) {
			element.click();
		}
	}

	public void uncheckTheCheckbox(WebElement driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		if (element.isSelected()) {
			element.click();
		}
	}

	public boolean isControlDisplayed(WebDriver driver, String locator) {
		Date date = new Date();
		log.info("Started time = " + date.toString());
		WebElement element = driver.findElement(By.xpath(locator));
		log.info("---------- Control displayed = " + locator);
		log.info("End time = " + date.toString());
		return element.isDisplayed();
	}

	public boolean isControlDisplayed(WebDriver driver, String locator, String... value) {
		locator = String.format(locator, (Object[]) value);
		Date date = new Date();
		log.info("Started time = " + date.toString());
		WebElement element = driver.findElement(By.xpath(locator));
		log.info("---------- Control displayed = " + locator);
		log.info("End time = " + date.toString());
		return element.isDisplayed();
	}

	public boolean isControlUndisplayed(WebDriver driver, String locator) {
		Date date = new Date();
		log.info("Started time = " + date.toString());
		overrideGlobalTimeout(driver, 10);
		List<WebElement> elements = driver.findElements(By.xpath(locator));
		if (elements.size() == 0) {
			date = new Date();
			log.info("End time = " + date.toString());
			return true;
		} else {
			date = new Date();
			log.info("End time = " + date.toString());
			return false;
		}
	}

	public boolean isControlUndisplayed(WebDriver driver, String locator, String... value) {
		Date date = new Date();
		log.info("Started time = " + date.toString());
		overrideGlobalTimeout(driver, 5);
		locator = String.format(locator, (Object[]) value);
		List<WebElement> elements = driver.findElements(By.xpath(locator));
		if (elements.size() == 0) {
			date = new Date();
			log.info("End time = " + date.toString());
			return true;
		} else {
			date = new Date();
			log.info("End time = " + date.toString());
			return false;
		}
	}

	public void overrideGlobalTimeout(WebDriver driver, long timeOut) {
		driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
	}

	public boolean isControlSelected(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isSelected();
	}

	public boolean isControlEnabled(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		return element.isEnabled();
	}

	public void acceptAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	public void cancelAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		alert.dismiss();
	}

	public String getTextAlert(WebDriver driver) {
		Alert alert = driver.switchTo().alert();
		return alert.getText();
	}

	public void sendKeyToAlert(WebDriver driver, String value) {
		Alert alert = driver.switchTo().alert();
		alert.sendKeys(value);
	}

	public void switchToWindowByID(WebDriver driver, String parentID) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runwindow : allWindows) {
			System.out.println("Window ID =" + runwindow);
			if (!runwindow.equals(parentID)) {
				driver.switchTo().window(runwindow);
				break;
			}
		}
	}

	public void switchToWindowByTitle(WebDriver driver, String title) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runwindows : allWindows) {
			System.out.println("Window ID =" + runwindows);
			driver.switchTo().window(runwindows);
			String currentTitle = driver.getTitle();
			if (currentTitle.equals(title)) {
				break;
			}
		}
	}

	public boolean closeAllWithoutParentWindows(WebDriver driver, String parentWindow) {
		Set<String> allWindows = driver.getWindowHandles();
		for (String runWindows : allWindows) {
			if (!runWindows.equals(parentWindow)) {
				driver.switchTo().window(runWindows);
				driver.close();
			}
		}
		driver.switchTo().window(parentWindow);
		if (driver.getWindowHandles().size() == 1)
			return true;
		else
			return false;
	}

	public void swichToIframe(WebDriver driver, String locator) {
		WebElement iframe = driver.findElement(By.xpath(locator));
		driver.switchTo().frame(iframe);
	}

	public void doubleClickToElement(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.doubleClick(element);

	}

	public void hoverMouseToElement(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.moveToElement(element).perform();
	}

	public void rightClickToElement(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		Actions action = new Actions(driver);
		action.contextClick(element).perform();
	}

	public void drapAndDrop(WebDriver driver, String sourceLocator, String targetLocator) {
		WebElement sourceElement = driver.findElement(By.xpath(sourceLocator));
		WebElement targetElement = driver.findElement(By.xpath(targetLocator));
		Actions action = new Actions(driver);
		action.dragAndDrop(sourceElement, targetElement).build().perform();
	}

	public void uploadBySendKey(WebDriver driver, String locator, String filepath) {
		WebElement element = driver.findElement(By.xpath(locator));
		element.sendKeys(filepath);
	}

	public void uploadByAutoIt(WebDriver driver, String locator, String filePath, String browserPath)
			throws IOException {
		WebElement element = driver.findElement(By.xpath(locator));
		element.click();
		Runtime.getRuntime().exec(new String[] { browserPath, filePath });
	}

	public void uploadByRobot(WebDriver driver, String filePath, String locator)
			throws AWTException, InterruptedException {
		// Specify the file location with extension
		StringSelection select = new StringSelection(filePath);

		// Copy to clipboard
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(select, null);

		// Click
		WebElement addfile = driver.findElement(By.xpath(locator));
		addfile.click();
		Thread.sleep(4000);

		Robot robot = new Robot();
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);

		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);

		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		Thread.sleep(1000);

		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
	}

	public Object executeForBrowserElement(WebDriver driver, String javaSript) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript(javaSript);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object executeForWebElement(WebDriver driver, WebElement element) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object scrollToBottomPage(WebDriver driver) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object scrollToElement(WebDriver driver, String locator) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			WebElement element = driver.findElement(By.xpath(locator));
			return js.executeScript("arguments[0].scrollIntoView(true);", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	public Object hightlightElement(WebDriver driver, String locator) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			WebElement element = driver.findElement(By.xpath(locator));
			return js.executeScript("arguments[0].style.border='2px groove green'", element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}
	}

	// public Object removeAttributeInDOM(WebDriver driver, WebElement element,
	// String attribute) {
	//
	// try {
	// JavascriptExecutor js = (JavascriptExecutor) driver;
	// return js.executeScript("arguments[0].removeAttribute('" + attribute + "');",
	// element);
	// } catch (Exception e) {
	// e.getMessage();
	// return null;
	// }
	// }

	// public Object removeAttributeInDOM(WebDriver driver, String locator, String
	// attribute) {
	// WebElement element = driver.findElement(By.xpath(locator));
	// try {
	// JavascriptExecutor js = (JavascriptExecutor) driver;
	// return js.executeScript("arguments[0].value='" + attribute + "';", element);
	// } catch (Exception e) {
	// e.getMessage();
	// return null;
	// }
	// }
	public void removeAtrributeInDOM(WebDriver driver, String locator, String attribute) {
		WebElement element = driver.findElement(By.xpath(locator));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].removeAttribute('" + attribute + "');", element);
	}

	public Object checkAnyImageLoaded(WebDriver driver, WebElement element, String attribute) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			return (boolean) js.executeScript(
					"return arguments[0].complete && "
							+ "typeof arguments[0].naturalWidth != 'undefined' && arguments[0].naturalWidth > 0",
					element);
		} catch (Exception e) {
			e.getMessage();
			return null;
		}

	}

	public void waitForControlPresenxe(WebDriver driver, String locator) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		By by = By.xpath(locator);
		wait.until(ExpectedConditions.presenceOfElementLocated(by));
	}

	public void waitForControlVisible(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public void waitForControlVisible(WebDriver driver, String locator, String... value) {
		locator = String.format(locator, (Object[]) value);
		log.info("---------- Wait for element visible = " + locator);
		WebElement element = driver.findElement(By.xpath(locator));
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	public void waitForControlClickable(WebDriver driver, String locator) {
		WebElement element = driver.findElement(By.xpath(locator));
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void waitForControlInvisible(WebDriver driver, String locator) {
		By by = By.xpath(locator);
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
	}

	public void waitForAlertPresence(WebDriver driver) {
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.alertIsPresent());
	}

	public NewCustomerPageObject openNewCustomerPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS, "New Customer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS, "New Customer");
		return PageManageDriver.getNewCustomerPage(driver);
	}

	public NewAccountPageObject openNewAccountPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS, "New Account");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS, "New Account");
		return PageManageDriver.getNewAccountPage(driver);
	}

	public EditCustomerPageObject openEditCustomerPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS, "Edit Customer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS, "Edit Customer");
		return PageManageDriver.getEditCustomerPage(driver);
	}

	public HomePageObject openHomePage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS, "Manager");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS, "Manager");
		return PageManageDriver.getHomePage(driver);
	}

	public DepositPageObject openDepositPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS, "Deposit");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS, "Deposit");
		return PageManageDriver.getDepositPage(driver);
	}

	public WithdrawalPageObject openWithdrawPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS, "Withdrawal");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS, "Withdrawal");
		return PageManageDriver.getWithdrawPage(driver);
	}

	public FunTransferPageObject openFunTranserPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS, "Fund Transfer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS, "Fund Transfer");
		return PageManageDriver.getFunTransferPage(driver);
	}

	public BalancePageObject openBalancePage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS, "Balance Enquiry");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS, "Balance Enquiry");
		return PageManageDriver.getBalancePage(driver);
	}
	
	public DeleteAccountPageObject openDeleteAccountPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS, "Delete Account");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS, "Delete Account");
		return PageManageDriver.getDeleteAccountPage(driver);
	}
	
	public DeleteCustomerPageObject openDeleteCustomerPage(WebDriver driver) {
		waitForControlVisible(driver, AbstractPageUI.DYNAMIC_LINKS, "Delete Customer");
		clickToElement(driver, AbstractPageUI.DYNAMIC_LINKS, "Delete Customer");
		return PageManageDriver.getDeleteCustomerPage(driver);
	}

}
