package pageObjects;

import org.openqa.selenium.WebDriver;

import bankguru.HomePageUI;
import commons.AbstractPage;

public class HomePageObject extends AbstractPage {

	WebDriver driver;

	public HomePageObject(WebDriver mapDriver) {
		driver = mapDriver;
	}

	public boolean verifyWelcomeMessageDisplayed() {
		waitForControlVisible(driver, HomePageUI.WELCOME_MESSAGE);
		return isControlDisplayed(driver, HomePageUI.WELCOME_MESSAGE);
	}

}
//a