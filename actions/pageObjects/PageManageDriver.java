package pageObjects;

import org.openqa.selenium.WebDriver;

public class PageManageDriver {

	WebDriver driver;
	private static LoginPageObject loginPage;
	private static RegisterPageObject registerPage;
	private static NewCustomerPageObject newCustomerPage;
	private static EditCustomerPageObject editCustomer;
	private static HomePageObject homePage;
	private static NewAccountPageObject newAccount;
	private static DepositPageObject depositPage;
	private static WithdrawalPageObject withdrawPage;
	private static FunTransferPageObject funTransferPage;
	private static BalancePageObject balancePage;
	private static DeleteAccountPageObject deleteAccountPage;
	private static DeleteCustomerPageObject deleteCustomerPage;

	public static LoginPageObject getLoginPage(WebDriver driver) {

		if (loginPage == null) {
			return new LoginPageObject(driver);
		} 
		return loginPage;
	}

	public static RegisterPageObject getRegisterPage(WebDriver driver) {

		if (registerPage == null) {
			return new RegisterPageObject(driver);
		}
		return registerPage;
	}

	public static NewCustomerPageObject getNewCustomerPage(WebDriver driver) {

		if (newCustomerPage == null) {
			return new NewCustomerPageObject(driver);
		}
		return newCustomerPage;
	}

	public static EditCustomerPageObject getEditCustomerPage(WebDriver driver) {

		if (editCustomer == null) {
			return new EditCustomerPageObject(driver);
		}
		return editCustomer;
	}

	public static HomePageObject getHomePage(WebDriver driver) {

		if (homePage == null) {
			return new HomePageObject(driver);
		}
		return homePage;
	}

	public static NewAccountPageObject getNewAccountPage(WebDriver driver) {

		if (newAccount == null) {
			return new NewAccountPageObject(driver);
		}
		return newAccount;
	}

	public static DepositPageObject getDepositPage(WebDriver driver) {

		if (depositPage == null) {
			return new DepositPageObject(driver);
		}
		return depositPage;
	}

	public static WithdrawalPageObject getWithdrawPage(WebDriver driver) {

		if (withdrawPage == null) {
			return new WithdrawalPageObject(driver);
		}
		return withdrawPage;
	}

	public static FunTransferPageObject getFunTransferPage(WebDriver driver) {
		
		if (funTransferPage == null) {
			return new FunTransferPageObject(driver);
		}
		return funTransferPage;
	}
	public static BalancePageObject getBalancePage(WebDriver driver) {

		if (balancePage == null) {
			return new BalancePageObject(driver);
		}
		return balancePage;
	}
	
	public static DeleteAccountPageObject getDeleteAccountPage(WebDriver driver) {

		if (deleteAccountPage == null) {
			return new DeleteAccountPageObject(driver);
		}
		return deleteAccountPage;
	}
	
	public static DeleteCustomerPageObject getDeleteCustomerPage(WebDriver driver) {

		if (deleteCustomerPage == null) {
			return new DeleteCustomerPageObject(driver);
		}
		return deleteCustomerPage;
	}
	
}
//c