package pageObjects;

import org.openqa.selenium.WebDriver;

import bankguru.NewAccountPageUI;
import commons.AbstractPage;

public class NewAccountPageObject extends AbstractPage {

	WebDriver driver;

	public NewAccountPageObject(WebDriver mapDriver) {
		driver = mapDriver;
	}

	public void inputToDynamicTextbox(String dynamicTextbox, String value) {
		waitForControlVisible(driver, NewAccountPageUI.DYNAMIC_TEXTBOX, dynamicTextbox, value);
		notClearAndSendKeyToElement(driver, NewAccountPageUI.DYNAMIC_TEXTBOX, value, dynamicTextbox);

	}

	public void clicktoDynamicTextbox(String dynamicTextbox) {
		waitForControlVisible(driver, NewAccountPageUI.DYNAMIC_TEXTBOX, dynamicTextbox);
		clickToElement(driver, NewAccountPageUI.DYNAMIC_TEXTBOX, dynamicTextbox);

	}

	public String getPayersAccount() {
		waitForControlVisible(driver, NewAccountPageUI.ACCOUNT_ID);
		return getTextElement(driver, NewAccountPageUI.ACCOUNT_ID);

	}
	
	public String getCurrentAmmountNewAccount() {
		waitForControlVisible(driver, NewAccountPageUI.CURENT_AMMOUNT);
		return getTextElement(driver, NewAccountPageUI.CURENT_AMMOUNT);

	}
	
	public boolean verifyAccountCreateMsg() {
		waitForControlVisible(driver, NewAccountPageUI.ACCOUNT_CREATE_SUCESS_MSG);
		return isControlDisplayed(driver, NewAccountPageUI.ACCOUNT_CREATE_SUCESS_MSG);
	}
	
}
//a