package pageObjects;

import org.openqa.selenium.WebDriver;

import bankguru.DepositPageUI;
import commons.AbstractPage;

public class DepositPageObject extends AbstractPage {

	WebDriver driver;

	public DepositPageObject(WebDriver mapDriver) {
		driver = mapDriver;
	}

	public void notClearAndinputToDynamicTextbox(String dynamicTextbox, String value) {
		waitForControlVisible(driver, DepositPageUI.DYNAMIC_OBJECT, dynamicTextbox, value);
		notClearAndSendKeyToElement(driver, DepositPageUI.DYNAMIC_OBJECT, value, dynamicTextbox);

	}

	public void clicktoDynamicButton(String dynamicButton) {
		waitForControlVisible(driver, DepositPageUI.DYNAMIC_OBJECT, dynamicButton);
		clickToElement(driver, DepositPageUI.DYNAMIC_OBJECT, dynamicButton);

	}

	public boolean verifyDepositSuccessMsg(String account) {
		waitForControlVisible(driver, DepositPageUI.DEPOSIT_SUCESS_MSG,account);
		return isControlDisplayed(driver, DepositPageUI.DEPOSIT_SUCESS_MSG,account);
	}

	public String getCurrentBalanceAtDepositPage() {
		waitForControlVisible(driver, DepositPageUI.CURREN_BALANCE);
		return getTextElement(driver, DepositPageUI.CURREN_BALANCE);

	}
}
//d