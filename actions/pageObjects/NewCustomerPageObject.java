package pageObjects;

import org.openqa.selenium.WebDriver;

import bankguru.NewCustomerPageUI;
import commons.AbstractPage;

public class NewCustomerPageObject extends AbstractPage {

	WebDriver driver;

	public NewCustomerPageObject(WebDriver mapDriver) {
		driver = mapDriver;
	}

	public boolean isDynamicErrorMessageDisplayed(String dynamicName, String dynamicErrorMessage) {
		waitForControlVisible(driver, NewCustomerPageUI.DYNAMIC_ERROR_MESSAGES, dynamicName, dynamicErrorMessage);
		return isControlDisplayed(driver, NewCustomerPageUI.DYNAMIC_ERROR_MESSAGES);

	}

	public void clicktoDynamicTextbox(String dynamicTextbox) {
		waitForControlVisible(driver, NewCustomerPageUI.DYNAMIC_TEXTBOX, dynamicTextbox);
		clickToElement(driver, NewCustomerPageUI.DYNAMIC_TEXTBOX, dynamicTextbox);

	}

	public void inputToDynamicTextbox(String dynamicTextbox, String value) {
		waitForControlVisible(driver, NewCustomerPageUI.DYNAMIC_TEXTBOX, dynamicTextbox, value);
		notClearAndSendKeyToElement(driver, NewCustomerPageUI.DYNAMIC_TEXTBOX, value, dynamicTextbox);

	}

	public void inputToDynamicTextArea(String dynamicTextArea, String value) {
		waitForControlVisible(driver, NewCustomerPageUI.DYNAMIC_TEXTAREA, dynamicTextArea, value);
		notClearAndSendKeyToElement(driver, NewCustomerPageUI.DYNAMIC_TEXTAREA, value, dynamicTextArea);

	}

	public void clicktoDynamicRadio(String dynamicRadio) {
		waitForControlVisible(driver, NewCustomerPageUI.DYNAMIC_RADIO, dynamicRadio);
		clickToElement(driver, NewCustomerPageUI.DYNAMIC_RADIO, dynamicRadio);

	}

	public String getCustomerID() {
		waitForControlVisible(driver, NewCustomerPageUI.CUSTOMER_ID);
		return getTextElement(driver, NewCustomerPageUI.CUSTOMER_ID);

	}

	public boolean verifyCreateCustomerSuccessfullyMsg() {
		waitForControlVisible(driver, NewCustomerPageUI.CREATE_USER_SUCCESSFULLY_MESSAGE);
		return isControlDisplayed(driver, NewCustomerPageUI.CREATE_USER_SUCCESSFULLY_MESSAGE);
	}
}

//a