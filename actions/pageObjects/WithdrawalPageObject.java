package pageObjects;

import org.openqa.selenium.WebDriver;

import bankguru.WithdrawalPageUI;
import commons.AbstractPage;

public class WithdrawalPageObject extends AbstractPage {

	WebDriver driver;

	public WithdrawalPageObject(WebDriver mapDriver) {
		driver = mapDriver;
	}

	public void notClearAndinputToDynamicTextbox(String dynamicTextbox, String value) {
		waitForControlVisible(driver, WithdrawalPageUI.DYNAMIC_OBJECT, dynamicTextbox, value);
		notClearAndSendKeyToElement(driver, WithdrawalPageUI.DYNAMIC_OBJECT, value, dynamicTextbox);

	}

	public void clicktoDynamicButton(String dynamicButton) {
		waitForControlVisible(driver, WithdrawalPageUI.DYNAMIC_OBJECT, dynamicButton);
		clickToElement(driver, WithdrawalPageUI.DYNAMIC_OBJECT, dynamicButton);

	}

	public String getCurrentBalanceWithdrawPage() {
		waitForControlVisible(driver, WithdrawalPageUI.CURREN_BALANCE);
		return getTextElement(driver, WithdrawalPageUI.CURREN_BALANCE);

	}
	
	public boolean verifyWithdrawalSuccessMsg(String account) {
		waitForControlVisible(driver, WithdrawalPageUI.WITHDRAW_SUCESS_MSG,account);
		return isControlDisplayed(driver, WithdrawalPageUI.WITHDRAW_SUCESS_MSG,account);
	}
}
//a