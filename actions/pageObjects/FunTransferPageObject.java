package pageObjects;

import org.openqa.selenium.WebDriver;

import bankguru.FunTransferPageUI;
import commons.AbstractPage;

public class FunTransferPageObject extends AbstractPage {

	WebDriver driver;

	public FunTransferPageObject(WebDriver mapDriver) {
		driver = mapDriver;
	}

	public void notClearAndinputToDynamicTextbox(String dynamicTextbox, String value) {
		waitForControlVisible(driver, FunTransferPageUI.DYNAMIC_OBJECT, dynamicTextbox, value);
		notClearAndSendKeyToElement(driver, FunTransferPageUI.DYNAMIC_OBJECT, value, dynamicTextbox);

	}

	public void notClearAndinputToDynamicAcountNo(String dynamicAccountNo, String value) {
		waitForControlVisible(driver, FunTransferPageUI.DYNAMIC_ACCOUNT_NO, dynamicAccountNo, value);
		notClearAndSendKeyToElement(driver, FunTransferPageUI.DYNAMIC_ACCOUNT_NO, value, dynamicAccountNo);

	}

	public void clicktoDynamicButton(String dynamicButton) {
		waitForControlVisible(driver, FunTransferPageUI.DYNAMIC_OBJECT, dynamicButton);
		clickToElement(driver, FunTransferPageUI.DYNAMIC_OBJECT, dynamicButton);

	}
	
	public boolean verifyFunTransferMsg( ) {
		waitForControlVisible(driver, FunTransferPageUI.FUN_TRANSFER_MSG);
		return isControlDisplayed(driver, FunTransferPageUI.FUN_TRANSFER_MSG);
	}
	
	public String getCurrentAmmount() {
		waitForControlVisible(driver, FunTransferPageUI.CURREN_AMMOUNT);
		return getTextElement(driver, FunTransferPageUI.CURREN_AMMOUNT);

	}
}
//a