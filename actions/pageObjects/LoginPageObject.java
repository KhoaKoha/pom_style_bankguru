package pageObjects;

import org.openqa.selenium.WebDriver;

import bankguru.LoginPageUI;
import commons.AbstractPage;

public class LoginPageObject extends AbstractPage {
	WebDriver driver;

	public LoginPageObject(WebDriver mapDriver) {
		driver = mapDriver;
	}

	public String getLoginUrl() {
		return getCurrentUrl(driver);
	}

	public RegisterPageObject clickRegisterPage() {
		waitForControlVisible(driver, LoginPageUI.REGISTER_LINK);
		clickToElement(driver, LoginPageUI.REGISTER_LINK);
		return PageManageDriver.getRegisterPage(driver);
	}

	public void inputUserId(String USERID) {
		waitForControlVisible(driver, LoginPageUI.USER_ID_TEXTBOX);
		sendKeyToElement(driver, LoginPageUI.USER_ID_TEXTBOX, USERID);
	}

	public void inputPassword(String PASSWORD) {
		waitForControlVisible(driver, LoginPageUI.PASSWORD_TEXTBOX);
		sendKeyToElement(driver, LoginPageUI.PASSWORD_TEXTBOX, PASSWORD);
	}

	public HomePageObject clickLoginButton() {
		waitForControlVisible(driver, LoginPageUI.LOGIN_BUTTON);
		clickToElement(driver, LoginPageUI.LOGIN_BUTTON);
		return PageManageDriver.getHomePage(driver);
	}

}
//a