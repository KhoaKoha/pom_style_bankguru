package pageObjects;

import org.openqa.selenium.WebDriver;

import bankguru.BalancePageUI;
import commons.AbstractPage;

public class DeleteAccountPageObject extends AbstractPage {

	WebDriver driver;

	public DeleteAccountPageObject(WebDriver mapDriver) {
		driver = mapDriver;
	}

	public void notClearAndinputToDynamicTextbox(String dynamicTextbox, String value) {
		waitForControlVisible(driver, BalancePageUI.DYNAMIC_OBJECT, dynamicTextbox, value);
		notClearAndSendKeyToElement(driver, BalancePageUI.DYNAMIC_OBJECT, value, dynamicTextbox);

	}

	public void clicktoDynamicButton(String dynamicButton) {
		waitForControlVisible(driver, BalancePageUI.DYNAMIC_OBJECT, dynamicButton);
		clickToElement(driver, BalancePageUI.DYNAMIC_OBJECT, dynamicButton);

	}

}
//b