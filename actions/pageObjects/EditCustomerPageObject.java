package pageObjects;

import org.openqa.selenium.WebDriver;

import bankguru.EditCustomerPageUI;
import commons.AbstractPage;

public class EditCustomerPageObject extends AbstractPage {

	WebDriver driver;

	public EditCustomerPageObject(WebDriver mapDriver) {
		driver = mapDriver;
	}


	public void clicktoDynamicObject(String dynamicObject) {
		waitForControlVisible(driver, EditCustomerPageUI.DYNAMIC_OBJECT, dynamicObject);
		clickToElement(driver, EditCustomerPageUI.DYNAMIC_OBJECT, dynamicObject);

	}

	public void inputToDynamicObject(String dynamicObject, String value) {
		waitForControlVisible(driver, EditCustomerPageUI.DYNAMIC_OBJECT, dynamicObject, value);
		ClearAndSendKeyToElement(driver, EditCustomerPageUI.DYNAMIC_OBJECT, value, dynamicObject);

	}

	public void inputToDynamicTextArea(String dynamicTextArea, String value) {
		waitForControlVisible(driver, EditCustomerPageUI.DYNAMIC_TEXTAREA, dynamicTextArea, value);
		ClearAndSendKeyToElement(driver, EditCustomerPageUI.DYNAMIC_TEXTAREA, value, dynamicTextArea);

	}

	public boolean verifyUpdateCustomerInfoSucessMsg() {
		waitForControlVisible(driver, EditCustomerPageUI.CUSTOMER_UPDATE_TEXT);
		return isControlDisplayed(driver, EditCustomerPageUI.CUSTOMER_UPDATE_TEXT);
	}
}
//e