package pageObjects;

import org.openqa.selenium.WebDriver;

import bankguru.BalancePageUI;
import bankguru.DeleteCustomerPageUI;
import commons.AbstractPage;

public class DeleteCustomerPageObject extends AbstractPage {

	WebDriver driver;

	public DeleteCustomerPageObject(WebDriver mapDriver) {
		driver = mapDriver;
	}

	public void notClearAndinputToDynamicTextbox(String dynamicTextbox, String value) {
		waitForControlVisible(driver, DeleteCustomerPageUI.DYNAMIC_OBJECT, dynamicTextbox, value);
		notClearAndSendKeyToElement(driver, DeleteCustomerPageUI.DYNAMIC_OBJECT, value, dynamicTextbox);

	}

	public void clicktoDynamicButton(String dynamicButton) {
		waitForControlVisible(driver, DeleteCustomerPageUI.DYNAMIC_OBJECT, dynamicButton);
		clickToElement(driver, DeleteCustomerPageUI.DYNAMIC_OBJECT, dynamicButton);

	}

}
//c