package pageObjects;

import org.openqa.selenium.WebDriver;

import bankguru.BalancePageUI;
import commons.AbstractPage;

public class BalancePageObject extends AbstractPage {

	WebDriver driver;

	public BalancePageObject(WebDriver mapDriver) {
		driver = mapDriver;
	}

	public void notClearAndinputToDynamicTextbox(String dynamicTextbox, String value) {
		waitForControlVisible(driver, BalancePageUI.DYNAMIC_OBJECT, dynamicTextbox, value);
		notClearAndSendKeyToElement(driver, BalancePageUI.DYNAMIC_OBJECT, value, dynamicTextbox);

	}

	public void clicktoDynamicButton(String dynamicButton) {
		waitForControlVisible(driver, BalancePageUI.DYNAMIC_OBJECT, dynamicButton);
		clickToElement(driver, BalancePageUI.DYNAMIC_OBJECT, dynamicButton);

	}

	public String getBalanceValue() {
		waitForControlVisible(driver, BalancePageUI.BALANCE);
		return getTextElement(driver, BalancePageUI.BALANCE);

	}

	public boolean verifyBalanceMsg(String account) {
		waitForControlVisible(driver, BalancePageUI.BALANCE_MSG, account);
		return isControlDisplayed(driver, BalancePageUI.BALANCE_MSG, account);
	}
}

//a