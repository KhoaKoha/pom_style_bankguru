package com.bankguru.login;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import commons.AbstractTest;
import pageObjects.EditCustomerPageObject;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.NewAccountPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.PageManageDriver;
import pageObjects.RegisterPageObject;

public class Login_02_CreateAccountAndLoginToApplication_POM extends AbstractTest {
	WebDriver driver;
	String loginUrl, email, userId, password;

	@Parameters("browser")
	@BeforeClass
	public void beforeClass(String browserName) {

		driver = openMultiBrowser(browserName);
		email = "automaticfc" + emailRandom() + "@gmail.com";

	}

	@Test
	public void TC_01_CreateAnAccount() {
		loginPage = PageManageDriver.getLoginPage(driver);
		loginUrl = loginPage.getLoginUrl();

		registerPage = loginPage.clickRegisterPage();
		registerPage.inputEmail(email);
		registerPage.clickToSubmitRegister();

		userId = registerPage.getLoginInfo();
		password = registerPage.getpasswordInfo();

	}

	@Test
	public void TC_02_LoginWithAboveInformation() {
		loginPage = registerPage.openLoginPage(loginUrl);
		loginPage.inputUserId(userId);
		loginPage.inputPassword(password);

		homePage = loginPage.clickLoginButton();

		Assert.assertTrue(homePage.verifyWelcomeMessageDisplayed());

		// Assert.assertTrue(driver.findElement(By.xpath("//td[text()='Manger Id : " +
		// userId + "']")).isDisplayed());

		// Step 4: open New Customer page
		newCustomerPage = homePage.openNewCustomerPage(driver);
		
		
		newCustomerPage.clicktoDynamicTextbox("name");
		newCustomerPage.clicktoDynamicTextbox("city");
		
		newCustomerPage.isDynamicErrorMessageDisplayed("name","Customer name must not be blank");
		

		// Step 5: Open New accoutn PAge

		newAccountPage = newCustomerPage.openNewAccountPage(driver);

		// Step 6: Open EditCustomer PAge

		editCustomerPage = newAccountPage.openEditCustomerPage(driver);

		homePage = editCustomerPage.openHomePage(driver);
		
		newCustomerPage = homePage.openNewCustomerPage(driver);
		
		newCustomerPage.openEditCustomerPage(driver);

	}

	@AfterClass
	public void afterClass() {

		driver.quit();
	}

	private LoginPageObject loginPage;
	private RegisterPageObject registerPage;
	private HomePageObject homePage;
	private NewCustomerPageObject newCustomerPage;
	private EditCustomerPageObject editCustomerPage;
	private NewAccountPageObject newAccountPage;
}
