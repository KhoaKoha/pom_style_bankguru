package com.bankguru.payment;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import bankguru.NewAccountPageUI;
import bankguru.NewCustomerPageUI;
import commons.AbstractTest;
import pageObjects.BalancePageObject;
import pageObjects.DeleteAccountPageObject;
import pageObjects.DeleteCustomerPageObject;
import pageObjects.DepositPageObject;
import pageObjects.EditCustomerPageObject;
import pageObjects.FunTransferPageObject;
import pageObjects.HomePageObject;
import pageObjects.LoginPageObject;
import pageObjects.NewAccountPageObject;
import pageObjects.NewCustomerPageObject;
import pageObjects.PageManageDriver;
import pageObjects.RegisterPageObject;
import pageObjects.WithdrawalPageObject;

public class Payment_Function extends AbstractTest {
	WebDriver driver;
	String loginUrl, email, loginID, password, customerID;
	String customer_email = "automationabc" + commons.AbstractTest.emailRandom() + "@gmail.com";
	String edit_email = "testing" + commons.AbstractTest.emailRandom() + "@gmail.com";
	String PayersAccountID;
	String PayeersAccountID = "46540";
	int initDeposit = 50000;
	int currentBalance = 0;
	int transferMoney = 5000;
	int withdrawMoney = 15000;
	int currentAmmount;
	int transferAmmount = 10000;

	@Parameters({"browser","version"})
	@BeforeClass

	public void beforeClass(String browserName,String browserVersion) {

		driver = openMultiBrowsersWithVersion(browserName,browserVersion);
		email = "khoatest" + emailRandom() + "@gmail.com";

		loginPage = PageManageDriver.getLoginPage(driver);
		loginUrl = loginPage.getLoginUrl();

		log.info("Open Login Page");
		registerPage = loginPage.clickRegisterPage();

		log.info("Input Email");
		registerPage.inputEmail(email);

		log.info("Click submit register");
		registerPage.clickToSubmitRegister();

		loginID = registerPage.getLoginInfo();
		password = registerPage.getpasswordInfo();

	}

	@Test
	public void TC_01_CreateNewCustomer() {

		loginPage = registerPage.openLoginPage(loginUrl);

		log.info("Payment_01 - Step_01 : Open Login page");
		loginPage = PageManageDriver.getLoginPage(driver);

		log.info("Payment_01 - Step_02 : Input email");
		loginPage.inputUserId(loginID);

		log.info("Payment_01 - Step_03 : Input password");
		loginPage.inputPassword(password);

		log.info("Payment_01 - Step_04 : Submit");
		homePage = loginPage.clickLoginButton();

		log.info("Payment_01 - Step_05 : Open New Customer page");
		newCustomerPage = homePage.openNewCustomerPage(driver);
		PageManageDriver.getNewCustomerPage(driver);

		log.info("Payment_01 - Step_06 : Input name");
		newCustomerPage.inputToDynamicTextbox("name", commons.Constant.CUSTOMER_NAME);

		log.info("Payment_01 - Step_07 : Input Gender");
		newCustomerPage.clicktoDynamicRadio("f");

		log.info("Payment_01 - Step_09 : Input day of birth");

		newCustomerPage.removeAtrributeInDOM(driver, NewCustomerPageUI.DOB_TXT, "type");
		newCustomerPage.inputToDynamicTextbox("dob", commons.Constant.CUSTOMER_DOB);

		log.info("Payment_01 - Step_10 : Input address");
		newCustomerPage.inputToDynamicTextArea("addr", commons.Constant.CUSTOMER_NAME);

		log.info("Payment_01 - Step_11 : Input city");
		newCustomerPage.inputToDynamicTextbox("city", commons.Constant.CUSTOMER_CITY);

		log.info("Payment_01 - Step_12 : Input state");
		newCustomerPage.inputToDynamicTextbox("state", commons.Constant.CUSTOMER_STATE);

		log.info("Payment_01 - Step_13 : Input PIN");
		newCustomerPage.inputToDynamicTextbox("pinno", commons.Constant.CUSTOMER_PIN);

		log.info("Payment_01 - Step_14 : Input Mobile");
		newCustomerPage.inputToDynamicTextbox("telephoneno", commons.Constant.CUSTOMER_MOBILE);

		log.info("Payment_01 - Step_15 : Input Email");
		newCustomerPage.inputToDynamicTextbox("emailid", customer_email);

		log.info("Payment_01 - Step_16 : Input Passwprd");
		newCustomerPage.inputToDynamicTextbox("password", commons.Constant.CUSTOMER_PASSWORD);

		log.info("Payment_01 - Step_17 : Input Submit");
		newCustomerPage.clicktoDynamicTextbox("sub");
		verifyTrue(newCustomerPage.verifyCreateCustomerSuccessfullyMsg());

		customerID = newCustomerPage.getCustomerID();

	}

	@Test
	public void TC_02_EditCreatedCustomer() {

		log.info("Payment_02 - Step_01 : Open NewCustomer Page");
		editCustomerPage = newCustomerPage.openEditCustomerPage(driver);
		editCustomerPage = PageManageDriver.getEditCustomerPage(driver);

		log.info("Payment_02 - Step_02 : Input CustomerID");
		editCustomerPage.inputToDynamicObject("cusid", customerID);

		log.info("Payment_02 - Step_03 : Click Submit");
		editCustomerPage.clicktoDynamicObject("AccSubmit");

		log.info("Payment_02 - Step_04 : Input Address");
		editCustomerPage.inputToDynamicTextArea("addr", commons.Constant.EDIT_ADDRESS);

		log.info("Payment_02 - Step_05 : Input City");
		editCustomerPage.inputToDynamicObject("city", commons.Constant.EDIT_CITY);

		log.info("Payment_02 - Step_06 : Input State");
		editCustomerPage.inputToDynamicObject("state", commons.Constant.EDIT_STATE);

		log.info("Payment_02 - Step_07 : Input PIN");
		editCustomerPage.inputToDynamicObject("pinno", commons.Constant.EDIT_PIN);

		log.info("Payment_02 - Step_08 : Input Telephone");
		editCustomerPage.inputToDynamicObject("telephoneno", commons.Constant.EDIT_MOBILE);

		log.info("Payment_02 - Step_09 : Input Email");
		editCustomerPage.inputToDynamicObject("emailid", edit_email);

		log.info("Payment_02 - Step_10 : Input Submit");
		editCustomerPage.clicktoDynamicObject("sub");

		log.info("Payment_02 - Step_11 : Verify Efit Success");
		verifyTrue(editCustomerPage.verifyUpdateCustomerInfoSucessMsg());
	}

	@Test
	public void TC_03_CreateNewAccountAndCheckDeposit() {

		log.info("Payment_03 - Step_01 : Open New Account Page");
		newAccountPage = editCustomerPage.openNewAccountPage(driver);
		newAccountPage = PageManageDriver.getNewAccountPage(driver);

		log.info("Payment_03 - Step_02: Input CustomerID");
		newAccountPage.inputToDynamicTextbox("cusid", customerID);

		log.info("Payment_03 - Step_03 : Select Account Type");
		newAccountPage.selectItemInDropdown(driver, NewAccountPageUI.SELECT_ACCOUNT_DROPDOWN, "Current");

		log.info("Payment_03 - Step_04 : Input Init Deposit value");
		newAccountPage.inputToDynamicTextbox("inideposit", String.valueOf(initDeposit));

		log.info("Payment_03 - Step_05 : Click Submit Button");
		newAccountPage.clicktoDynamicTextbox("button2");

		log.info("Payment_03 - Step_06 : Get Payer Account");
		PayersAccountID = newAccountPage.getPayersAccount();

		log.info("Payment_03 - Step_07 : Verify Account Create Success");
		verifyTrue(newAccountPage.verifyAccountCreateMsg());

		currentAmmount = initDeposit + currentBalance;

		log.info("Payment_03 - Step_08 : Verify Init Deposit and Current Ammout are Equal");
		verifyEquals(newAccountPage.getCurrentAmmountNewAccount(), String.valueOf(currentAmmount));

	}

	@Test
	public void TC_04_TransferMoneyAndCheckBalance() {

		log.info("Payment_04 - Step_01 : Open Deposit Page");
		depositPage = newAccountPage.openDepositPage(driver);
		depositPage = PageManageDriver.getDepositPage(driver);

		log.info("Payment_04 - Step_02 : Input AccountID");
		depositPage.notClearAndinputToDynamicTextbox("accountno", PayersAccountID);

		log.info("Payment_04 - Step_03 : Input Transfer Money Value");
		depositPage.notClearAndinputToDynamicTextbox("ammount", String.valueOf(transferMoney));

		log.info("Payment_04 - Step_04 : Input Deposit Descripntion");
		depositPage.notClearAndinputToDynamicTextbox("desc", "Deposit");

		log.info("Payment_04 - Step_05 : Click Submit");
		depositPage.clicktoDynamicButton("AccSubmit");

		log.info("Payment_04 - Step_06 : Verify Deposit Success");
		verifyTrue(depositPage.verifyDepositSuccessMsg(PayersAccountID));

		log.info("Payment_04 - Step_07 : Check Current Balance");
		currentBalance = initDeposit + transferMoney;

		log.info("Payment_04 - Step_08 : Verify Current Balance and Ammount are equal");
		verifyEquals(depositPage.getCurrentBalanceAtDepositPage(), String.valueOf(currentBalance));

	}

	@Test
	public void TC_05_WithDrawMoneyFromCurrentAccountAndCheck() {

		log.info("Payment_05 - Step_01 : Open Withdraw Page");
		withdrawPage = depositPage.openWithdrawPage(driver);
		withdrawPage = PageManageDriver.getWithdrawPage(driver);

		log.info("Payment_05 - Step_02 : Input Account ID");
		withdrawPage.notClearAndinputToDynamicTextbox("accountno", PayersAccountID);

		log.info("Payment_05 - Step_03 : Input Withdraw Ammount");
		withdrawPage.notClearAndinputToDynamicTextbox("ammount", String.valueOf(withdrawMoney));

		log.info("Payment_05 - Step_04 : Input Withdraw Description");
		withdrawPage.notClearAndinputToDynamicTextbox("desc", "Withdraw");

		log.info("Payment_05 - Step_05 : Click Submit");
		withdrawPage.clicktoDynamicButton("AccSubmit");

		log.info("Payment_05 - Step_06 : Check Current Balance");
		currentBalance = currentBalance - withdrawMoney;

		log.info("Payment_05 - Step_07 : Verify Withdraw Money Success");
		verifyTrue(withdrawPage.verifyWithdrawalSuccessMsg(PayersAccountID));

		log.info("Payment_05 - Step_18 : Verify Current Balance is 40000");
		verifyEquals(withdrawPage.getCurrentBalanceWithdrawPage(), String.valueOf(currentBalance));
	}

	@Test
	public void TC_06_TransferMoneyAndCheckBalance() {

		log.info("Payment_06 - Step_01 : Open Tranfer Page");
		funTranserPage = withdrawPage.openFunTranserPage(driver);
		funTranserPage = PageManageDriver.getFunTransferPage(driver);

		log.info("Payment_06 - Step_02 : Input Payer ID");
		funTranserPage.notClearAndinputToDynamicAcountNo("message10", PayersAccountID);

		log.info("Payment_06 - Step_03 : Input Payeer ID");
		funTranserPage.notClearAndinputToDynamicAcountNo("message11", PayeersAccountID);

		log.info("Payment_06 - Step_04 : Input Transfer Ammount");
		funTranserPage.notClearAndinputToDynamicTextbox("ammount", String.valueOf(transferAmmount));

		log.info("Payment_06 - Step_05 : Input Description");
		funTranserPage.notClearAndinputToDynamicTextbox("desc", "Transfer");

		log.info("Payment_06 - Step_06 : Click Submit");
		funTranserPage.clicktoDynamicButton("AccSubmit");

		log.info("Payment_06 - Step_07 : Verify Transfer Moeny Success ");
		verifyTrue(funTranserPage.verifyFunTransferMsg());

		log.info("Payment_06 - Step_08 : Verify Transfer Ammount is 10000");
		verifyEquals(funTranserPage.getCurrentAmmount(), String.valueOf(transferAmmount));

	}

	@Test
	public void TC_07_CheckPayerAccountBalance() {

		log.info("Payment_07 - Step_01 : Open Balance Page");
		balancePage = funTranserPage.openBalancePage(driver);
		balancePage = PageManageDriver.getBalancePage(driver);

		log.info("Payment_07 - Step_02 : Input PayerID");
		balancePage.notClearAndinputToDynamicTextbox("accountno", String.valueOf(PayersAccountID));

		log.info("Payment_07 - Step_03 : Click Submit");
		balancePage.clicktoDynamicButton("AccSubmit");

		log.info("Payment_07 - Step_04 : Verify Balance Page Displayed");
		verifyTrue(balancePage.verifyBalanceMsg(PayersAccountID));

		log.info("Payment_07 - Step_05 : Check Balance");
		currentBalance = currentBalance - transferAmmount;

		log.info("Payment_07 - Step_06 : Verify Balance is 30000");
		verifyEquals(balancePage.getBalanceValue(), String.valueOf(currentBalance));

	}

	@Test
	public void TC_08()  {

		log.info("Payment_08 - Step_01 : Open Delete Account Page");
		deleteAccountPage = balancePage.openDeleteAccountPage(driver);
		deleteAccountPage = PageManageDriver.getDeleteAccountPage(driver);

		log.info("Payment_08 - Step_02 : Input PayerID");
		deleteAccountPage.notClearAndinputToDynamicTextbox("accountno", String.valueOf(PayersAccountID));

		log.info("Payment_08 - Step_03 : Click Submit");
		deleteAccountPage.clicktoDynamicButton("AccSubmit");

		log.info("Payment_08 - Step_04 : Confirm Delete Account - Alert");
		deleteAccountPage.acceptAlert(driver);
		String deleteAccountSuccess = deleteAccountPage.getTextAlert(driver);
		

		log.info("Payment_08 - Step_05 : Verify Account Delete Successfully Alert");
		verifyEquals(deleteAccountSuccess, "Account Deleted Sucessfully");

		log.info("Payment_08 - Step_06 : Close Alert");
		deleteAccountPage.acceptAlert(driver);

	}

	@Test
	public void TC_09()  {

		log.info("Payment_09 - Step_01 : Open Delete Customer Page");
		deleteCustomerPage = deleteAccountPage.openDeleteCustomerPage(driver);
		deleteCustomerPage = PageManageDriver.getDeleteCustomerPage(driver);

		log.info("Payment_09 - Step_02 : Input CustomerID");
		deleteCustomerPage.notClearAndinputToDynamicTextbox("cusid", String.valueOf(customerID));

		log.info("Payment_09 - Step_03 : Click Submit");
		deleteCustomerPage.clicktoDynamicButton("AccSubmit");

		log.info("Payment_09 - Step_04 : Confirm Delete Customer Alert");
		deleteCustomerPage.acceptAlert(driver);

		String deleteCustomerSuccess = deleteCustomerPage.getTextAlert(driver);

		log.info("Payment_09 - Step_05 : Verify Delete Customer Successfully");
		verifyEquals(deleteCustomerSuccess, "Customer deleted Successfully");

		log.info("Payment_09 - Step_06 : Close Alerts");
		deleteAccountPage.acceptAlert(driver);
	}

	@AfterClass
	public void afterClass() {

		closeBrowser(driver);
	}

	private RegisterPageObject registerPage;
	private LoginPageObject loginPage;
	private HomePageObject homePage;
	private NewCustomerPageObject newCustomerPage;
	private EditCustomerPageObject editCustomerPage;
	private NewAccountPageObject newAccountPage;
	private DepositPageObject depositPage;
	private WithdrawalPageObject withdrawPage;
	private FunTransferPageObject funTranserPage;
	private BalancePageObject balancePage;
	private DeleteAccountPageObject deleteAccountPage;
	private DeleteCustomerPageObject deleteCustomerPage;
}
