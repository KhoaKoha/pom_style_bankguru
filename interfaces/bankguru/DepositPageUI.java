package bankguru;

public class DepositPageUI {

	public static final String DYNAMIC_OBJECT = "//input[@name='%s']";
	public static final String DEPOSIT_SUCESS_MSG = "//p[contains(text(),'Transaction details of Deposit for Account %s')]";
	public static final String CURREN_BALANCE = "//td[text()='Current Balance']/following-sibling::td";

}
