package bankguru;

public class WithdrawalPageUI {

	public static final String DYNAMIC_OBJECT = "//input[@name='%s']";
	public static final String WITHDRAW_SUCESS_MSG = "//p[contains(text(),'Transaction details of Withdrawal for Account %s')]";
	public static final String CURREN_BALANCE = "//td[text()='Current Balance']/following-sibling::td";
	
}
