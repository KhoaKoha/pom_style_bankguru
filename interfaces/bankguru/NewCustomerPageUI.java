package bankguru;

public class NewCustomerPageUI {

	public static final String DYNAMIC_ERROR_MESSAGES = "//input[@name='%s']/following-sibling::label[text()='%s']";
	public static final String DYNAMIC_TEXTBOX = "//input[@name='%s']";
	public static final String DYNAMIC_TEXTAREA = "//textarea[@name='%s']";
	public static final String DYNAMIC_RADIO = "//input[@value='%s']";
	public static final String CREATE_USER_SUCCESSFULLY_MESSAGE = "//p[text()='Customer Registered Successfully!!!']";
	public static final String CUSTOMER_ID = "//td[text()='Customer ID']/following-sibling::td";
	public static final String DOB_TXT = "//input[@name = 'dob']";
}
