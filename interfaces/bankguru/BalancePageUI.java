package bankguru;

public class BalancePageUI {

	public static final String DYNAMIC_OBJECT = "//input[@name='%s']";
	public static final String BALANCE_MSG = "//p[contains(text(),'Balance Details for Account %s')]";
	public static final String BALANCE = "//td[text()='Balance']/following-sibling::td";
	
}
