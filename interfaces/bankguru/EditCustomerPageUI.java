package bankguru;

public class EditCustomerPageUI {

	public static final String DYNAMIC_OBJECT = "//input[@name='%s']";
	public static final String DYNAMIC_TEXTAREA = "//textarea[@name='%s']";
	public static final String CUSTOMER_UPDATE_TEXT ="//p[text()='Customer details updated Successfully!!!']";
}
