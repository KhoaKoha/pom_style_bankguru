package bankguru;

public class NewAccountPageUI {

	public static final String DYNAMIC_TEXTBOX = "//input[@name='%s']";
	public static final String SELECT_ACCOUNT_DROPDOWN= "//select[@name='selaccount']";
	public static final String DYNAMIC_DROPDOWN = "//option[text()='%s']";
	public static final String ACCOUNT_ID = "//td[text()='Account ID']/following-sibling::td";
	public static final String CURENT_AMMOUNT = "//td[text()='Current Amount']/following-sibling::td";
	public static final String ACCOUNT_CREATE_SUCESS_MSG ="//p[text()='Account Generated Successfully!!!']";
	
	
}
