package bankguru;

public class FunTransferPageUI {

	public static final String DYNAMIC_OBJECT = "//input[@name='%s']";
	public static final String DYNAMIC_ACCOUNT_NO = "//label[@id='%s']/preceding-sibling::input";
	public static final String FUN_TRANSFER_MSG = "//p[contains(text(),'Fund Transfer Details')]";
	public static final String CUSTOMER_UPDATE_TEXT ="//p[text()='Fund Transfer Details']";
	public static final String CURREN_AMMOUNT = "//td[text()='Amount']/following-sibling::td";
}
